#include <iostream>
#include <onnxruntime_cxx_api.h>
#include <onnxruntime_cxx_inline.h>
#include <onnxruntime_session_options_config_keys.h>

using namespace std;

int main(){
    Ort::Env env;
	Ort::RunOptions runOptions;
	Ort::Session session(nullptr);

    std::cout << __cplusplus << " Test done"  << endl;
    auto modelPath = L"/Users/acordeir/Documents/LPNHE/ATLAS/MA5/ONNX_runtime/cpp-onnxruntime-resnet-console-app-main/OnnxRuntimeResNet\\assets\\resnet50v2.onnx";
    return 0;
}