// OnnxRuntimeResNet.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <onnxruntime_cxx_api.h>
#include <iostream>

static Ort::Session test(std::string modelPath){
    Ort::Env env;
	Ort::RunOptions runOptions;
	Ort::Session session(nullptr);

    session = Ort::Session(env, modelPath.c_str(), Ort::SessionOptions{ nullptr });


    return session;
}

int main()
{
	//Ort::Env env;
	Ort::RunOptions runOptions;
	//Ort::Session session(nullptr);

    std::string modelPath = "/Users/acordeir/Documents/LPNHE/ATLAS/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_4jets.onnx";

    // create session
    //session = Ort::Session(env, modelPath.c_str(), Ort::SessionOptions{ nullptr });
    auto session = test(modelPath);
    Ort::Env env;

    std::cout << "Session creation" << std::endl;

    // Check Input shapes 

    const size_t num_input_nodes = session.GetInputCount();

    std::cout << "Number of input nodes " << num_input_nodes << std::endl;

    auto num_output_nodes = session.GetOutputCount();

    std::cout << "Number of output nodes " << num_output_nodes << std::endl;


    // define names
    Ort::AllocatorWithDefaultOptions ort_alloc;

    Ort::AllocatedStringPtr inputName = session.GetInputNameAllocated(0, ort_alloc);  
    Ort::AllocatedStringPtr outputName = session.GetOutputNameAllocated(0, ort_alloc);

    const std::array<const char*,1> inputNames = {inputName.get()};
    const std::array<const char*,1> outputNames = {outputName.get()};

    std::cout << "Input : name =" << inputName.get() << std::endl;
    std::cout << "Input : data =" << inputNames.data() << std::endl;

    std::cout << "Output : name =" << outputName.get() << std::endl;
    std::cout << "Output : data =" << outputNames.data() << std::endl;


    // print input node types
    auto type_info = session.GetInputTypeInfo(0);
    auto tensor_info = type_info.GetTensorTypeAndShapeInfo();

    auto type = tensor_info.GetElementType();

    // print input shapes/dims
    auto input_node_dims = tensor_info.GetShape();
    
    std::cout << "Input : type = " << type << std::endl;
    std::cout << "Input : num_dims = " << input_node_dims.size() << std::endl;
    
    auto input_tensor_size =1;


    for (size_t j = 0; j < input_node_dims.size(); j++) {
      std::cout << "Input : dim[" << j << "] = " << input_node_dims[j] << std::endl;
      input_tensor_size *=input_node_dims[j];
    }

    std::cout << "input_node_dims data = " << input_node_dims.data() << " size = " << input_node_dims.size() << std::endl;
    std::cout << " input_tensor_size " << input_tensor_size << std::endl ;


    // Define input arrays 
    std::array<float, 65> input;
    //std::vector<std::vector<float>*> input;

    for(size_t i=0; i< 65;i++){
        input[i]=2.0;
    }
    std::cout << "Input size" << input.size() << std::endl;
    std::cout << "Input : " ;
    for (auto i : input){
        std::cout << i << ", ";
    }
    std::cout << std ::endl;


    std::vector<float> nn_output;
    std::array<float, 1> results;

    // define shape
    const std::array<int64_t, 2> inputShape = {1,65};
    const std::array<int64_t, 2> outputShape = { 1,1 };




    // define Tensor
    auto memory_info = Ort::MemoryInfo::CreateCpu(OrtDeviceAllocator, OrtMemTypeCPU);

    auto inputTensor = Ort::Value::CreateTensor<float>(memory_info, input.data(), input_tensor_size, inputShape.data(), inputShape.size());
    auto outputTensor = Ort::Value::CreateTensor<float>(memory_info, results.data(), results.size(), outputShape.data(), outputShape.size());

    // run inference
    session.Run(runOptions, inputNames.data(), &inputTensor, 1, outputNames.data(), &outputTensor, 1);
    try {
        session.Run(runOptions, inputNames.data(), &inputTensor, 1, outputNames.data(), &outputTensor, 1);
    }
    catch (Ort::Exception& e) {
        std::cout << e.what() << std::endl;
        return 1;
    }

    std::cout <<"after run" << std::endl;

    std::cout << "Results size :" << results.size() << std::endl;
    std::cout << "Result : " << results[0] << std::endl;



}    